const express = require("express");
const router = express.Router();
const app = express();
const styles = require("./styles");
require("dotenv").config();

router.get("/styles/:id", (req, res, next) => {
  const id = req.params.id;
  if (!id) next();
  res.json(styles[id]);
});
app.use("/api/v1", router);

app.listen(process.env.PORT, () => {
  console.log(`Server listens on ${process.env.PORT}`);
});
