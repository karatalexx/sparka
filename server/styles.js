const styles = {
  1: {
    textColor: "#FFFFFF",
    bgColor: "#000000",
    fontSize: "s", //14px
    fontFamily: "Arial"
  },
  2: {
    textColor: "#FFFFFF",
    bgColor: "#FF0000",
    fontSize: "m", //18px
    fontFamily: "Arial"
  }
};

module.exports = styles;
