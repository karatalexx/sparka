import { Component, ViewChild, AfterViewInit, OnInit } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';
import { filter, take, shareReplay } from 'rxjs/operators';
import { KonvaComponent } from 'ng2-konva';
import Konva from 'konva';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  @ViewChild('stage', { static: false }) stage: KonvaComponent;
  @ViewChild('layer', { static: false }) layer: KonvaComponent;

  public title = 'sparka';
  private isAnimating: any;
  private titleTween: Konva.Tween;
  private supportTween: Konva.Tween;
  public loading: boolean = true;

  public configStage: Observable<any> = of({
    width: 480,
    height: 270
  });

  public constructor() {
    const subj = new Subject<boolean>();
    this.isAnimating = subj.pipe(shareReplay(1));
    this.pushDefaultLoadingState();
  }

  ngAfterViewInit() {
    const layer: Konva.Layer = this.layer.getStage() as Konva.Layer;

    const titleText = new Konva.Text({
      text: 'My very first title'.toUpperCase(),
      width: 320,
      fontFamily: 'Arial',
      fontSize: 28,
      fontStyle: 'bold',
      padding: 10,
      fill: 'white',
      align: 'center',
      strokeWidth: 1,
      shadowForStrokeEnabled: false,
      letterSpacing: 0
    });
    const supportText = new Konva.Text({
      text: 'some support text',
      fontFamily: 'Arial',
      fontSize: 24,
      padding: 2,
      align: 'center',
      fill: 'white',
      draggable: true,
      strokeWidth: 1,
      shadowForStrokeEnabled: false,
      letterSpacing: 0
    });

    const titleLabel = new Konva.Label({
      id: 'title',
      x: this.stage.getStage().getWidth() / 2,
      y: this.stage.getStage().getHeight() / 2,
      opacity: 1,
      draggable: true
    });
    const offsetX = titleText.width() / 2;
    const offsetY = titleText.height() / 2;
    titleLabel.offset({ x: offsetX, y: offsetY });
    titleLabel.scale({ x: 0, y: 0 });

    titleLabel.add(
      new Konva.Tag({
        stroke: '#fff',
        strokeWidth: 6
      })
    );

    const supportLabel = new Konva.Label({
      id: 'support',
      x: -supportText.width(),
      y: this.stage.getStage().getHeight() / 2 - supportText.height() / 2 + 50,
      opacity: 1
    });

    supportLabel.add(
      new Konva.Tag({
        id: 'supportTag'
      })
    );

    const backgroundImage = new Image();
    backgroundImage.onload = () => {
      const background = new Konva.Image({
        id: 'backgroundImage',
        x: 0,
        y: 0,
        image: backgroundImage,
        width: this.stage.getStage().getWidth(),
        height: this.stage.getStage().getHeight()
      });
      layer.add(background);
      titleLabel.add(titleText);

      supportLabel.add(supportText);

      layer.add(titleLabel);
      layer.add(supportLabel);

      this.textEditor(titleLabel, layer);
      this.textEditor(supportLabel, layer);

      background.cache();
      background.filters([Konva.Filters.Brighten]);
      background.brightness(-0.2);
      layer.batchDraw();
      this.loading = false;
      this.setTweenAnimation();
    };
    backgroundImage.src = '/assets/background-sparka.jpeg';
  }

  public startAnimation() {
    this.isAnimating.next(true);
    this.titleTween.play();
  }

  public resetAnimation() {
    this.isAnimating
      .pipe(
        filter(val => !val),
        take(1)
      )
      .subscribe(() => {
        this.titleTween.reset();
      });
  }

  private setTweenAnimation() {
    const layer: Konva.Layer = this.layer.getStage();
    const title = layer.findOne('#title');
    const support = layer.findOne('#support');

    this.supportTween = new Konva.Tween({
      node: support,
      duration: 0.4,
      easing: Konva.Easings.EaseIn,
      x: this.stage.getStage().getWidth() / 2 - support.width() / 2,
      onFinish: () => {
        this.isAnimating.next(false);
      },
      onReset: () => {
        support.setAttr('x', -support.width());
        console.log(support);
      }
    });
    this.titleTween = new Konva.Tween({
      node: title,
      duration: 0.3,
      easing: Konva.Easings.EaseIn,
      scaleX: 1,
      scaleY: 1,
      onFinish: () => {
        this.supportTween.play();
      },
      onReset: () => {
        this.supportTween.reset();
        layer.batchDraw();
      }
    });
  }

  private pushDefaultLoadingState() {
    this.isAnimating.pipe(take(1)).subscribe();
    this.isAnimating.next(false);
  }

  private textEditor(textNode, layer) {
    const stage = layer.getStage();
    textNode.on('dblclick', () => {
      textNode.hide();
      layer.batchDraw();
      const text = textNode.getText();

      const textPosition = text.absolutePosition();
      const stageBox = stage.container().getBoundingClientRect();

      const areaPosition = {
        x: stageBox.left + textPosition.x,
        y: stageBox.top + textPosition.y
      };

      const textarea = document.createElement('textarea');
      document.body.appendChild(textarea);

      textarea.value = text.text();
      textarea.style.position = 'absolute';
      textarea.style.top = areaPosition.y + 8 + 'px';
      textarea.style.left = areaPosition.x + 10 + 'px';
      textarea.style.width = text.width() - text.padding() * 2 + 'px';
      textarea.style.height = text.height() - text.padding() * 2 + 5 + 'px';
      textarea.style.fontSize = text.fontSize() + 'px';
      textarea.style.border = 'none';
      textarea.style.padding = '0px';
      textarea.style.margin = '0px';
      textarea.style.overflow = 'hidden';
      textarea.style.background = 'none';
      textarea.style.outline = 'none';
      textarea.style.resize = 'none';
      textarea.style.lineHeight = text.lineHeight();
      textarea.style.fontWeight = text.fontStyle();
      textarea.style.fontFamily = text.fontFamily();
      textarea.style.textAlign = text.align();
      textarea.style.color = text.fill();
      textarea.style.backgroundColor = 'transparent';

      const isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;
      textarea.style.height = 'auto';
      textarea.style.height = textarea.scrollHeight + 3 + 'px';
      textarea.focus();

      function removeTextarea() {
        textarea.parentNode.removeChild(textarea);
        window.removeEventListener('click', handleOutsideClick);

        textNode.show();
        layer.batchDraw();
      }

      function setTextareaWidth(newWidth) {
        if (!newWidth) {
          newWidth = text.placeholder.length * text.fontSize();
        }

        const isSafari = /^((?!chrome|android).)*safari/i.test(navigator.userAgent);

        if (isSafari || isFirefox) {
          newWidth = Math.ceil(newWidth);
        }
        textarea.style.width = newWidth + 'px';
      }

      textarea.addEventListener('keydown', e => {
        if (e.keyCode === 13 && !e.shiftKey) {
          text.text(textarea.value);
          removeTextarea();
        }
        if (e.keyCode === 27) {
          removeTextarea();
        }
      });

      textarea.addEventListener('keydown', () => {
        const scale = text.getAbsoluteScale().x;
        setTextareaWidth(text.width() * scale);
        textarea.style.height = 'auto';
        textarea.style.height = textarea.scrollHeight + text.fontSize() + 'px';
      });

      function handleOutsideClick(e) {
        if (e.target !== textarea) {
          text.text(textarea.value);
          removeTextarea();
        }
      }

      setTimeout(() => {
        window.addEventListener('click', handleOutsideClick);
      });
    });
  }
}
